import time

from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.firefox.service import Service as FirefoxService
from webdriver_manager.firefox import GeckoDriverManager
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from webdriver_manager.microsoft import EdgeChromiumDriverManager
from selenium.webdriver.edge.service import Service as EdgeService
from selenium.webdriver.edge.options import Options as EdgeOptions


class Browser:
    # to run in headless mode instant should contain '--headless' parameter

    def __init__(self, chrome=None, edge=None, firefox=None, headless=None):
        self.chrome, self.edge, self.firefox, self.headless = chrome, edge, firefox, headless

    def create_driver(self):
        options = None
        if self.chrome:
            if self.headless:
                options = ChromeOptions()
                options.add_argument(self.headless)
            self.driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()), options=options)
            print('chrome initiated')
        elif self.edge:
            if self.headless:
                options = EdgeOptions()
                options.add_argument(self.headless)
            self.driver = webdriver.Edge(service=EdgeService(EdgeChromiumDriverManager().install()), options=options)
            print('edge initiated')
        elif self.firefox:
            if self.headless:
                options = FirefoxOptions()
                options.add_argument(self.headless)
            self.driver = webdriver.Firefox(service=FirefoxService(GeckoDriverManager().install()), options=options)
            print("firefox initiated")


# chrome = Browser(chrome=True)
# chrome.create_driver()
# chrome.driver.get("http://robotframework.org/Selenium2Library/Selenium2Library.html#Mouse%20Down")
# time.sleep(2)
# chrome.driver.close()
# #
# edge = Browser(edge=True, headless=False)
# edge.create_driver()
# edge.driver.get("https://github.com/")
# time.sleep(2)
# edge.driver.close()

browser = Browser(chrome=True)
browser.create_driver()
browser.driver.get("https://docs.pytest.org/en/stable/deprecations.html#calling-fixtures-directly")
time.sleep(2)
browser.driver.close()
